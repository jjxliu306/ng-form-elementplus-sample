# ng-form-elementplus-sample

ng-form 系列element-plus版本的示例

## 安装依赖
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

 