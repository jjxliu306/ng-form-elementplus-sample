import { createApp } from 'vue'
import App from './App.vue'  

const app = createApp(App)
 

import './style.css'
 

 



 // 导入组件库  

// 导入element-ui    
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css' 


import NgForm  from 'ng-form-elementplus'  
import 'ng-form-elementplus/lib/style.css'

// 注册组件库 
app.use(NgForm ,  {locale: 'en'})  
app.use(ElementPlus)  


import * as ElementPlusIconsVue from '@element-plus/icons-vue'
 
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}


app.mount('#app')
